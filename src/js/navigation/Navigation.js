class Navigation {
	constructor() {
		this.element_id   = 'navigation';
		this.element      = document.getElementById(this.element_id);
		this.active_class = this.element_id + '_opened';
		this.fade_id      = this.element_id + '-fade';
		this.button_id    = this.element_id + '-button';
		this.init();
	}

	init() {
		this.click_listener  = window.addEventListener('click', (event) => this.clickHandler(event));
		this.resize_listener = window.addEventListener('resize', (event) => this.resizeHandler(event));
	}

	clickHandler(event) {
		if (event.target.id == this.button_id || event.target.id == this.fade_id) {
			event.preventDefault();
			this.toggleActiveClass();
		}
	}
	
	toggleActiveClass() {
		this.element.classList.toggle(this.active_class);
	}

	resizeHandler() {
		this.removeActiveClass();
	}

	removeActiveClass() {
		this.element.classList.remove(this.active_class);
	}
}

export const navigation = new Navigation();