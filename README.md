# Тестовое задание от LuckyGroup
## Задача/результат

Задание: Адаптивная вёрстка по макету: https://www.figma.com/file/6m4uiRG7qPoJoKvpPYv42P/%D0%94%D0%B8%D0%B7%D0%B0%D0%B9%D0%BD-%D1%81%D0%B0%D0%B9%D1%82%D0%B0-%D1%87%D0%B5%D0%BC%D0%BF%D0%B8%D0%BE%D0%BD%D0%B0%D1%82%D0%B0?node-id=1-2&t=tE8AOveGDtarIepB-0 <br>
Превью: https://lidera.netlify.app/<br>

### Работа с проектом
Запуск локального сервера: npm run dev (http://localhost:9000/).<br>
Билд: npm run build.
