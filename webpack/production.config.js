const { merge } = require('webpack-merge');
const config_general = require('./config');

const config_prod = merge(config_general, {
	mode: 'production',
})


module.exports = new Promise((resolve, reject) => {
	resolve(config_prod);
});